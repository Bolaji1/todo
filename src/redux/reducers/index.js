import { ADD_TO_DO} from "../actions";


export const todos = (state = [], action) => {
    switch (action.type) {
        case ADD_TO_DO:
            return state.concat([{ text: action.text, complete: false}])


        default:
            return state

    }
}
