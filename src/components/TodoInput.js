import React, { Component } from 'react';
import { connect } from 'react-redux';
import {ADD_TO_DO} from "../redux/actions";


class TodoInput extends Component {
    state = {
        text: ''
    }

    handleOnChange = (e) => {
        this.setState({ text: e.target.value});
    }
    addTodo = () => {
       this.props.dispatch({ type:ADD_TO_DO, text: this.state.text })
        this.setState({ text: ''})
    }
    render(){
        return (
            <div>
                <input type="text" value={this.state.text} onChange={this.handleOnChange}/>
                <button onClick={this.addTodo}>Add Todo</button>
            </div>
        )
    }
}


export  default connect()(TodoInput);