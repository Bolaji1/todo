import React, { Component } from 'react';
import { connect } from 'react-redux';


class TodoList extends Component {
    render(){
        return (
            <div>
              <ul>{this.props.todos.map((todo, index) => {
                  return (
                      <li key={index}>{todo.text}</li>
                  )
              })}
              </ul>
            </div>
        )
    }
}

const mapStateToProps = (state) => (
    {
        todos: state
    }
)

export  default connect(mapStateToProps)(TodoList);