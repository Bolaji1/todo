import React, { Component } from 'react';
import TodoInput  from  './components/TodoInput'
import { Provider } from 'react-redux';
import  store  from './redux'
import './App.css';
import TodoList from "./components/TodoList";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Provider store={store}>
        <TodoInput/>
        <TodoList/>
        </Provider>
      </div>
    );
  }
}

export default App;
